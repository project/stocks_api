# Stocks API

This module provides APIs for retrieving financial and industry data for both individual stocks and stock exchanges.

## Features

### Stock Exchange API Services

The Stock Exchange API services make HTTP requests for stock exchange data.
This data includes a list of stocks traded on an exchange, along with some basic industry and financial data for each stock on the exchange, parsed into a map.
Currently, only stocks listed on the NASDAQ, AMEX, and NYSE are supported through the NASDAQ Exchange API implementation.

#### Usage

The NASDAQ Exchange API is a Drupal service that can be called from the server using drush, cron, or another calling module. The intention of this service is to be run periodicly to get the latest stocks on an exchange. Here is an example of using the NASDAQExchangeAPI service with drush to request a map of stocks listed on the NASDAQ stock exchange.

`drush ev 'print_r(\Drupal::service("stocks_api.exchanges")->requestExchangeSummaries());'`

The requestExchangeSummaries() function returns a CSV-formatted list of all stocks on an exchange, like so:

`...
"ZYXI","Zynex, Inc.","17.45","$605.61M","n/a","Health Care","Biotechnology: Electromedical & Electrotherapeutic Apparatus","https://old.nasdaq.com/symbol/zyxi",
"ZNGA","Zynga Inc.","9.32","$10.02B","2011","Technology","EDP Services","https://old.nasdaq.com/symbol/znga",
...`

This CSV can be parsed into a map using the buildStockMapFromExchangeSummaries() function, like so:

`drush ev 'print_r(\Drupal::service("stocks_api.exchanges")->buildStockMapFromExchangeSummaries(\Drupal::service("stocks_api.exchanges")->requestExchangeSummaries()));'`

This would return the same info parsed into an array map, like so:

`(
  ...
  [3781] => Array
    (
      [Symbol] => ZYXI
      [Name] => Zynex, Inc.
      [LastSale] => 17.45
      [MarketCap] => $605.61M
      [IPOyear] => n/a
      [Sector] => Health Care
      [industry] => Biotechnology: Electromedical & Electrotherapeutic Apparatus
      [Summary Quote] => https://old.nasdaq.com/symbol/zyxi
      [] =>
    )
  [3782] => Array
    (
      [Symbol] => ZNGA
      [Name] => Zynga Inc.
      [LastSale] => 9.32
      [MarketCap] => $10.02B
      [IPOyear] => 2011
      [Sector] => Technology
      [industry] => EDP Services
      [Summary Quote] => https://old.nasdaq.com/symbol/znga
      [] =>
    )
  ...
)`


### Stock Data API

The Stock Data APIs make HTTP requests for individual stock data. This data is returned as a JSON object and parsed into a map.

Currently, only the Alpha Vantage stock API is implemented for requesting data on individual stocks through the AlphaVantageStockAPI service.
The requests to Alpha Vantage require an API key, which must be generated from the Alpha Vantage website (https://www.alphavantage.co/).
The API key should then be stored on the server and referenced from settings, for security purposes, in the "alpha_vantage_api_key" setting in settings.php.

#### Usage

The Alpha Vangtage Stock API is a Drupal service that can be called from the server using drush, cron, or another calling module. The intention of this service is to be run periodicly to get the latest stock data, including prices. Here is an example of using the AlphaVantageStockAPI service with drush to request the latest data for Apple stock (ticker symbol: "AAPL").

`drush ev 'print_r(\Drupal::service("stocks_api.stocks")->requestStockQuote("AAPL"));'`

This would return a map of values, such as:

Array
(
    [01. symbol] => AAPL
    [02. open] => 112.8900
    [03. high] => 115.3700
    [04. low] => 112.2200
    [05. price] => 113.0200
    [06. volume] => 144711986
    [07. latest trading day] => 2020-10-02
    [08. previous close] => 116.7900
    [09. change] => -3.7700
    [10. change percent] => -3.2280%
)

### Extensibility

The module supports scalability to other stock data services through class interfaces.

### Future Development

A configuration form (StocksAPISettingsForm) has been added to support saving configuration for common uses of stock data. This configuration form does not currently make use of the saved values. In future versions of the Stocks API module, we will look to make use of the configuration form.

## Testing

This module has not been tested for production environments.

## Contributors

* **Omar Abed** (@omarabed)
* **Malik Kotob** (@malikkotob) - Drupal 8 Grandmaster

## Acknowledgments

Big thanks to @malikkotob for extensive and thorough guidance throughout the development of this module.
