# Changelog - Stocks API

Significant changes to the Stocks API module will be documented in this file.

## 2018-11-15
Initial commit of Stocks API. Includes:
- Integration with Alpha Vantage stock API for stock quotes.
- Integration with NASDAQ.com for NYSE and NASDAQ stock exchanges.
- API functions provided as Drupal services.

