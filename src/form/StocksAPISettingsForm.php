<?php

namespace Drupal\stocks_api\form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form for getting configuration settings for Stocks API.
 */
class StocksAPISettingsForm extends ConfigFormBase {

  /**
   * Return form ID.
   */
  public function getFormId() {
    return 'stocks_api_settings';
  }

  /**
   * Return configuration name.
   */
  public function getEditableConfigNames() {
    return [
      'stocks_api.settings',
    ];
  }

  /**
   * Construct form with fields and values from config.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('stocks_api.settings');

    $form['api_selection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Enable the Stock API to use for data updates.'),
    ];

    $form['api_selection']['api_select'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select Stock API'),
      '#default_value' => $config->get('api_select') ?? 0,
      '#options' => [
        0 => $this->t('AlphaVantage'),
      ],
      '#required' => TRUE,
      '#description' => $this->t('For security purposes, the API Key for the selected API should be stored in settings, in the "[api_name]_api_key" setting.'),
    ];

    $form['api_selection']['supported_exchanges'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Supported exchanges'),
      '#description' => $this->t('Which exchanges are supported by this application?'),
      '#options' => [
        'NYSE' => 'NYSE',
        'NASDAQ' => 'NASDAQ',
      ],
      '#default_value' => $config->get('supported_exchanges') ?? ['NYSE', 'NASDAQ'],
    ];

    $form['frequency_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Configure frequency of stock data updates'),
      '#description' => $this->t('How often should stock data be updated?'),
      '#open' => TRUE,
    ];

    $form['frequency_details']['request_rate_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum requests per minute'),
      '#description' => $this->t('Maximum number of API requests allowed (per minute) under the API Key. Use 0 for unlimited.'),
      '#default_value' => $config->get('request_rate_limit') ?? 0,
      '#required' => TRUE,
    ];

    $form['frequency_details']['data_expiration'] = [
      '#type' => 'number',
      '#title' => $this->t('Desired data refresh rate (in seconds).'),
      '#description' => $this->t('Ideally, how long is the stock data valid for before it should be updated with an API request?'),
      '#default_value' => $config->get('data_expiration') ?? 10,
      '#required' => TRUE,
    ];

    $form['frequency_details']['stock_list_update_frequency'] = [
      '#type' => 'number',
      '#title' => $this->t('How often (in days) to update the list of supported stocks.'),
      '#description' => $this->t('How often (in days) to update the list of supported stocks. This includes adding newly-listed stocks and purging delisted stocks.'),
      '#default_value' => $config->get('stock_list_update_frequency') ?? 7,
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit configuration form contents to config.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('stocks_api.settings');
    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();
    return parent::submitForm($form, $form_state);
  }

}
