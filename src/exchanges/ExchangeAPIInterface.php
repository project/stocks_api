<?php

namespace Drupal\stocks_api\exchanges;

/**
 * Provides an interface for defining API Interfaces to stock exchange info.
 *
 * @ingroup stocks_api
 */
interface ExchangeAPIInterface {

  /**
   * Requests summaries of the enabled exchanges.
   *
   * @return array
   *   Map of exchange contents
   */
  public function requestExchangeSummaries();

  /**
   * Parse exchange summary CSV into map.
   *
   * @param array $exchangeSummaries
   *   Map of exchange contents, in the following format:
   *      ['NYSE'] => '"Symbol", "Name", "LastSale", "MarketCap", "ADR TSO",
   *                   "IPOyear", "Sector", "Industry", "Summary Quote",
   *                   "YI", "111, Inc.", "9.35", "67086250", "7175000",
   *                   "2018", "Health Care", "Medical/Nursing Services",
   *                   "https://www.nasdaq.com/symbol/yi",'.
   *
   * @return array
   *   Map of exchange results, where each stock is parsed into a map
   */
  public function buildStockMapFromExchangeSummaries(array $exchangeSummaries);

}
